import { useCallback, useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import HeaderComponent from '../../../components/HeaderComponent';
import { motion } from "framer-motion";
import { Alert, AlertTitle, Button, Card, CardContent, CardHeader, Collapse, Container, Grid, IconButton } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import fields from '../fields';
import axios from 'axios';
import { apiUrl } from '../../../services/url';
import { useDispatch } from 'react-redux';
import { changeConstant } from '../../../features/constantSlice';
import CloseIcon from '@mui/icons-material/Close';
import { yupResolver } from "@hookform/resolvers/yup";
import { Controller, useForm } from "react-hook-form";
import * as yup from "yup";

const container = {
    hidden: { opacity: 1, scale: 0 },
    visible: {
        opacity: 1,
        scale: 1,
        transition: {
            delayChildren: 0.3,
            staggerChildren: 0.2
        }
    },
    animate: { transition: { delay: 0.6 } }
}

const item = {
    hidden: { y: 20, opacity: 0 },
    visible: {
        y: 0,
        opacity: 1
    }
}

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
    firstName: yup
        .string()
        .required("You must enter a firstname"),
    lastName: yup
        .string()
        .required("You must enter a lastname"),
    age: yup
        .number()
        .required("You must enter a age"),
});

const defaultValues = {
    firstName: "",
    lastName: "",
    age: 1,
    photo: ""
};

export default function ContactForm() {
    const navigate = useNavigate();
    const params = useParams();
    const dispatch = useDispatch();
    const { control, formState, handleSubmit, reset } = useForm({
        mode: "onChange",
        defaultValues,
        resolver: yupResolver(schema),
    });
    const { isValid, errors } = formState;

    const [errorAlert, setErrorAlert] = useState(false);

    const handleForm = useCallback((itemData) => {
        let dataItem = {};
        if (itemData) {
            fields.forEach(row => {
                if (row.type !== 'file') {
                    dataItem[row.id] = itemData[row.id]
                }
            })
            reset(dataItem)
        }
    }, [reset])

    useEffect(() => {
        if (params.id) {
            const getDataById = axios.get(`${apiUrl}/contact/${params.id}`);
            if (getDataById) {
                getDataById
                    .then(res => handleForm(res.data.data))
                    .catch(err => setErrorAlert(err))
            }
        }
    }, [handleForm, params.id])

    const onSubmit = async (data) => {
        try {
            let dataPost = {
                ...data,
                photo: 'http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550'
            }
            if (params.id) {
                await axios.put(`${apiUrl}/contact/${params.id}`, dataPost);
                dispatch(changeConstant({ alertType: 'success', alertSource: 'edit' }));
            } else {
                await axios.post(`${apiUrl}/contact`, dataPost);
                dispatch(changeConstant({ alertType: 'success', alertSource: 'add' }));
            }
            navigate(-1);
        } catch (error) {
            setErrorAlert("There is an error!")
        }
    }

    return (
        <Container maxWidth="xxl">
            <Collapse in={errorAlert ? true : false}>
                <div className='pl-12 pr-12 pt-5'>
                    <Alert
                        severity="error"
                        variant="filled"
                        action={
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    setErrorAlert(false);
                                }}
                            >
                                <CloseIcon fontSize="inherit" />
                            </IconButton>
                        }
                        sx={{ mb: 2 }}>
                        <AlertTitle>Error</AlertTitle>
                        {errorAlert}
                    </Alert>
                </div>
            </Collapse>
            <div className='mt-3 ml-12'>
                <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.2 } }}
                >
                    <HeaderComponent breadcrumbs={['Contact', 'Form']} />
                </motion.div>
            </div>
            <div className='p-12'>
                <Card>
                    <motion.div
                        initial={{ opacity: 0, x: 20 }}
                        animate={{ opacity: 1, x: 0, transition: { delay: 0.4 } }}
                    >
                        <CardHeader title="Form Contact" />
                    </motion.div>
                    <CardContent>
                        <motion.div
                            variants={container}
                            initial="hidden"
                            animate="visible"
                        >
                            <form
                                name="loginForm"
                                noValidate
                                onSubmit={handleSubmit(onSubmit)}
                            >
                                <Grid container spacing={2}>
                                    {fields.map((row, idx) => (
                                        <Grid item xs={12} md={6} key={idx}>
                                            <motion.div variants={item}>
                                                <Controller
                                                    name={row.id}
                                                    control={control}
                                                    render={({ field }) => (
                                                        <TextField
                                                            {...field}
                                                            fullWidth
                                                            label={row.label}
                                                            type={row.type}
                                                            variant="outlined"
                                                            name={row.id}
                                                            InputLabelProps={{
                                                                shrink: row.type === 'file' ? true : undefined,
                                                            }}
                                                            required={row.required}
                                                            error={!!errors[row.id]}
                                                            helperText={errors[row.id]?.message}
                                                        />
                                                    )}
                                                />
                                            </motion.div>
                                        </Grid>
                                    ))}
                                </Grid>
                                <motion.div
                                    initial={{ opacity: 0, x: 20 }}
                                    animate={{ opacity: 1, x: 0, transition: { delay: 0.4 } }}
                                >
                                    <div className='pb-12 pt-6'>
                                        <Button variant="contained" color='secondary' style={{ marginRight: '10px' }} onClick={() => navigate(-1)}>Cancel</Button>
                                        <Button
                                            type="submit"
                                            variant="contained"
                                            color="primary"
                                            aria-label="Sign in"
                                            disabled={!isValid}
                                            size="large"
                                        >Submit</Button>
                                    </div>
                                </motion.div>
                            </form>
                        </motion.div>
                    </CardContent>
                </Card>
            </div>
        </Container>
    );
}