import { Button, Container } from '@mui/material';
import TableComponent from '../../components/Table/TableComponent';
import AddIcon from '@mui/icons-material/Add';
import HeaderComponent from '../../components/HeaderComponent';
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import fields from './fields';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { apiUrl } from '../../services/url';
import { useDispatch, useSelector } from 'react-redux';
import { changeConstant } from '../../features/constantSlice';
import AlertComponent from '../../components/Alert';
import DialogConfirm from '../../components/DialogConfirm';


function ContactList() {
    const navigate = useNavigate();
    const columns = fields;
    const [data, setData] = useState([]);
    const [openDialog, setOpenDialog] = useState(false);
    const alertType = useSelector((state) => state.constant.alertType);
    const alertSource = useSelector((state) => state.constant.alertSource);
    const errorMessage = useSelector((state) => state.constant.errorMessage);
    const dispatch = useDispatch();

    const getDataTable = () => {
        const getData = axios.get(`${apiUrl}/contact`)
        if (getData) {
            getData
                .then(res => setData(res.data.data))
                .catch(err => console.log(err))
        }
    }

    useEffect(() => {
        getDataTable();
    }, [])

    useEffect(() => {
        if (alertType) {
            setTimeout(() => {
                dispatch(changeConstant({ alertType: null, alertSource: null, errorMessage: null }))
            }, 2000);
        }
    }, [alertType, dispatch])

    const actionDelete = async (evt, selected) => {
        try {
            setOpenDialog(false)
            await Promise.all(selected.map(async (id) => {
                await axios.delete(`${apiUrl}/contact/${id}`);
            }));
            dispatch(changeConstant({ alertType: 'success', alertSource: 'delete' }));
            getDataTable();
        } catch (error) {
            dispatch(changeConstant({ alertType: 'error', alertSource: 'delete', errorMessage: "Delete data error!" }));
        }
    }

    return (
        <Container maxWidth="xxl">
            <AlertComponent
                alertSource={alertSource}
                alertType={alertType}
                errorMessage={errorMessage}
                handleClose={() => {
                    dispatch(changeConstant({ alertSource: null, alertType: null }));
                }}
            />
            <DialogConfirm open={openDialog} handleClose={() => setOpenDialog(false)} handleConfirm={actionDelete} />
            <div className='mt-3 ml-12'>
                <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.2 } }}
                >
                    <HeaderComponent breadcrumbs={['Contact']} />
                </motion.div>
            </div>
            <div className="p-12">
                <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.4 } }}
                >
                    <div className='mb-3'>
                        <Button variant="contained" color="success" onClick={() => navigate('/form')}><AddIcon /> &nbsp; Add Contact</Button>
                    </div>
                </motion.div>
                <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.6 } }}
                >
                    <TableComponent
                        columns={columns}
                        data={data}
                        primaryKey="id"
                        label="Contact List"
                        actionDelete={() => setOpenDialog(true)}
                    />
                </motion.div>
            </div>
        </Container>
    );
}

export default ContactList;
