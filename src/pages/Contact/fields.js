const fields = [
    {
        id: 'firstName',
        numeric: false,
        disablePadding: true,
        label: 'Firstname',
        type: "text",
        required: true
    },
    {
        id: 'lastName',
        numeric: false,
        disablePadding: false,
        label: 'Lastname',
        type: "text",
        required: true
    },
    {
        id: 'age',
        numeric: false,
        disablePadding: false,
        label: 'Age',
        type: "number",
        required: true
    },
    {
        id: 'photo',
        numeric: false,
        disablePadding: false,
        label: 'Photo',
        type: "file"
    },
]

export default fields;