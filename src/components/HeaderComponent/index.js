import * as React from "react";
import Typography from "@mui/material/Typography";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import HomeIcon from '@mui/icons-material/Home';

function HeaderComponent(props) {
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <Typography
        color="textSecondary"
        to="/dashboard"
        style={{ textDecoration: "none" }}
      >
        <HomeIcon sx={{ mr: 0.5 }} />
        Home
      </Typography>
      {props.breadcrumbs.map((item, index) => {
        return (
          <div key={index}>
            <Typography color="textSecondary">{item}</Typography>
          </div>
        );
      })}
    </Breadcrumbs>
  );
}

export default HeaderComponent;
