import { render, screen } from '@testing-library/react';
import DialogConfirm from './index';

test('renders dialog confirm', () => {
    render(<DialogConfirm />);
    const dialog = screen.getByTestId("dialogconfirm");
    expect(dialog).toBeInTheDocument();
});
