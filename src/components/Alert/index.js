import { Alert, AlertTitle, Collapse, IconButton } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';

const AlertComponent = (props) => {
    const { alertSource, alertType, errorMessage, handleClose } = props;

    return (
        <Collapse in={alertType ? true : false}>
            <div className='pl-12 pr-12 pt-5'>
                <Alert
                    data-testid="alert"
                    variant="filled"
                    severity={alertType ?? "info"}
                    action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={handleClose}
                        >
                            <CloseIcon data-testid="closeicon" fontSize="inherit" />
                        </IconButton>
                    }
                    sx={{ mb: 2 }}>
                    <AlertTitle>{alertType !== 'success' ? "Error" : "Success"} </AlertTitle>
                    {alertType === 'error' ? errorMessage : `Data ${alertSource === 'add' ? 'addded' : alertSource === 'edit' ? 'changed' : "deleted"} successfully!`}

                </Alert>
            </div>
        </Collapse>
    )
}

export default AlertComponent;