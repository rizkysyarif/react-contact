import { render, screen } from '@testing-library/react';
import Alert from './index';

test('renders alert', () => {
    render(<Alert />);
    const alert = screen.getByTestId("alert");
    expect(alert).toBeInTheDocument();
});

test('renders close icon', () => {
    render(<Alert />);
    const closeicon = screen.getByTestId("closeicon");
    expect(closeicon).toBeInTheDocument();
});
