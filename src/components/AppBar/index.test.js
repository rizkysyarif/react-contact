import { render, screen } from '@testing-library/react';
import AppBarComponent from './index';

test('renders app bar', () => {
    render(<AppBarComponent />);
    const appBar = screen.getByTestId("appbar");
    expect(appBar).toBeInTheDocument();
});
