import { Suspense, lazy } from 'react';
import './App.css';
import AppBarComponent from './components/AppBar';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { ThemeProvider } from '@mui/material';
import { theme } from './styles/theme';

const Contact = lazy(() => import("./pages/Contact"));
const ContactForm = lazy(() => import("./pages/Contact/Form"));
const LoadingComponent = lazy(() => import("./components/Loading"))
function App() {
  return (
    <ThemeProvider theme={theme}>
      <AppBarComponent />
      <Router>
        <Suspense fallback={<LoadingComponent />}>
          <Routes>
            <Route
              path="/"
              element={<Contact />}
            />
            <Route
              path="/form/:id?"
              element={<ContactForm />}
            />
          </Routes>
        </Suspense>
      </Router>

    </ThemeProvider>
  );
}

export default App;
