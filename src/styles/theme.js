// import grey from "@material-ui/core/colors/grey";
// import { defaultBgColor, primaryColor } from "../constant/theme";
import { createTheme } from '@mui/material';
import { grey } from '@mui/material/colors';

export const theme = createTheme({
  palette: {
    // primary: {
    //   main: primaryColor,
    // },
    secondary: {
      main: grey[100],
    },
    // default: {
    //   main: defaultBgColor,
    // },
  },
});
