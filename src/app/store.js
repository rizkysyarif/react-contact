import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/constantSlice';

export const store = configureStore({
    reducer: {
        constant: counterReducer,
    },
})