import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    alertType: null,
    alertSource: null,
    errorMessage: null
}

export const constantSlice = createSlice({
    name: 'constant',
    initialState,
    reducers: {
        changeConstant: (state, action) => {
            return {
                ...state,
                ...action.payload
            }
        },
    },
})

// Action creators are generated for each case reducer function
export const { changeConstant } = constantSlice.actions

export default constantSlice.reducer