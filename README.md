# React Contact

> Simple CRUD contact
 
## Table of contents

> * [React Contact](#React-data-table)
>   * [Table of contents](#table-of-contents)
>   * [Introduction](#introduction)
>   * [Deployment](#deployment)
>   * [Features](#features)
>   * [Installation](#installation)
>     * [Clone](#clone)
>     * [Install Depedencies](#install-depedencies)
>     * [Run server development](#run-server-development)
>   * [Dependencies](#dependencies)
>   * [License](#license)

## Introduction

> This is simple CRUD Contact using ReactJS with pagination, sort column, select column, and validation form features.

## Deployment

> Live preview in https://master--react-contact-rizky.netlify.app/

## Features
  - Pagination
  - Sort Column
  - Select Column
  - CRUD
  - Validation Form
 
## Installation

### Clone
```
$ git clone https://gitlab.com/rizkysyarif/react-contact.git
$ cd react-contact
```

### Install Depedencies
```
npm install
```

### Run server development
```
npm start
```

## Dependencies
```
- Axios
- React Redux
- Redux Toolkit
- React Hook Form
- Material UI
- Framer-motion
- Yup
```

## License
MIT


 
 